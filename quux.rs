fn main () {
    println!("{}", -1 as uint);
    println!("{}", std::uint::MAX);
    // for maybeLine in io::stdin().lines() {
    //     match maybeLine {
    //         Err(_) => println!("error"),
    //         Ok(line) => print!("{}", line),
    //     }
    // }
    let mut v : Vec<int> = vec![3,5];
    *v.get_mut(0) = v[1];
    // the following results in a borrow error:
    // v.push(v[1]);
    // however this works???
    *v.get_mut(0) = v[0];
    // the following doesn't work either
    // let r = v.get_mut(0);
    // v.push(6);
    println!("{}", v);
}
