A folder for the little throwaway files I write to experiment.
I don't literally throw them away because you never know when they're going to prove useful.

See also https://github.com/nguyentito/miscalgo

