Random.self_init ();;

let draw_dice () = [|2;3;5;7;11;13|].(Random.int 6)

let experience () =
  let x = ref 1 and t = ref 0 in
  while !x < 2*3*5*7*11*13 do
    incr t;
    let y = draw_dice () in
    (if !x mod y <> 0 then x := !x * y)
  done;
  !t
  
  

