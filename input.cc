#include <iostream>
#include <vector>

using namespace std;

int main () {
    vector<vector<int> > t(3, vector<int>(3));
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            cin >> t[i][j];
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++)
            cout << t[i][j];
        cout << endl;
    }
    return 0;
}

