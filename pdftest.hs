import System.IO
import Pdf.Toolbox.Document
import qualified Data.ByteString as B
import Data.Traversable (forM)
import Control.Monad hiding (forM)
import Control.Applicative
import qualified Data.Text.IO as TIO

-- main = do
--   withBinaryFile "/home/tito/Dropbox/Cours/Géométrie différentielle.pdf" ReadMode $ \h -> do
--     x <- runPdfWithHandle h knownFilters $ do
--       info <- documentInfo =<< document
--       join <$> forM info infoTitle
--     case x of
--       Right (Just (Str bs)) -> B.putStrLn bs
--       Right Nothing -> putStrLn "no title"
--       Left err -> print err

main = do
  withBinaryFile "/home/tito/Dropbox/Cours/Géométrie différentielle.pdf" ReadMode $ \h -> do
    x <- runPdfWithHandle h knownFilters $ do
      rootNode <- catalogPageNode =<< documentCatalog =<< document
      -- according to the doc, this may be inefficient
      firstPage <- pageNodePageByNum rootNode 0
      pageExtractText firstPage
    case x of
      Right txt -> TIO.writeFile "viterbo" txt
      Left err -> print err
      
