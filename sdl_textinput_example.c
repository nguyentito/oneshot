﻿#include <stdio.h>
#include <string.h>

#include "SDL.h"
#include "SDL_ttf.h"

/* Global variables */
SDL_Renderer *renderer;
SDL_Texture *prompt_texture;
TTF_Font *bigfont, *mediumfont, *smallfont;
int prompt_w, prompt_h;
const SDL_Color black = {0, 0, 0};

/* This function is called by the event loop to update the screen */
void render(char* text, char* compo, char* hello_msg)
{
	SDL_Rect rect;
	SDL_Surface *temp_surface;
	SDL_Texture *temp_texture;
	
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
	SDL_RenderClear(renderer);

	rect.x = 20; rect.y = 20; rect.w = prompt_w; rect.h = prompt_h;
	SDL_RenderCopy(renderer, prompt_texture, NULL, &rect);

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	rect.x += rect.w + 20; rect.w = 780 - rect.x;
	SDL_RenderDrawRect(renderer, &rect);
	rect.x += 10; rect.y += (prompt_h - TTF_FontHeight(smallfont)) / 2;
	rect.w = 0;

	if (text[0] != '\0')
	{
		TTF_SetFontStyle(smallfont, TTF_STYLE_NORMAL);
		temp_surface = TTF_RenderUTF8_Blended(smallfont, text, black);
		temp_texture = SDL_CreateTextureFromSurface(renderer, temp_surface);

		rect.w = temp_surface->w; rect.h = temp_surface->h;
		SDL_RenderCopy(renderer, temp_texture, NULL, &rect);
		SDL_FreeSurface(temp_surface);
		SDL_DestroyTexture(temp_texture);
	}

	if (compo[0] != '\0')
	{
		TTF_SetFontStyle(smallfont, TTF_STYLE_UNDERLINE);
		temp_surface = TTF_RenderUTF8_Blended(smallfont, compo, black);
		temp_texture = SDL_CreateTextureFromSurface(renderer, temp_surface);
					
		rect.x += rect.w; rect.w = temp_surface->w; rect.h = temp_surface->h;
		SDL_RenderCopy(renderer, temp_texture, NULL, &rect);
		SDL_FreeSurface(temp_surface);					
		SDL_DestroyTexture(temp_texture);
	}

	if (hello_msg[0] != '\0')
	{
		temp_surface = TTF_RenderUTF8_Blended(bigfont, hello_msg, black);
		temp_texture = SDL_CreateTextureFromSurface(renderer, temp_surface);
		rect.x = (800 - temp_surface->w) / 2; rect.y = 580 - TTF_FontHeight(bigfont);
		rect.w = temp_surface->w; rect.h = TTF_FontHeight(bigfont);
		SDL_FreeSurface(temp_surface);
		SDL_RenderCopy(renderer, temp_texture, NULL, &rect);
		SDL_DestroyTexture(temp_texture);
	}

	SDL_RenderPresent(renderer);
}

/* Removes the last character from an UTF8-encoded string */
int backspace(char* str)
{
	char* last_char = NULL;
	char ascii_mask = 1 << 7; /* 10000000 */
	char multibyte_mask = 3 << 6; /* 11000000 */
	while (*str)
	{
		if (((ascii_mask & (*str)) == 0) ||
			((multibyte_mask & (*str)) == multibyte_mask))
		{
			last_char = str;
		}
		str++;
	}
	if (last_char != NULL)
	{
		*last_char = '\0';
		return 1;
	}
	return 0;
}

void event_loop()
{
	char text[256] = "";
	char compo[SDL_TEXTEDITINGEVENT_TEXT_SIZE] = "";
	char hello_msg[256] = "";
	
	int done = 0;
	SDL_Event evt;

	while (!done)
	{
		while (SDL_PollEvent(&evt))
		{
			switch (evt.type)
			{
			case SDL_QUIT:
				done = 1;
				break;
			case SDL_TEXTINPUT:
				strcat(text, evt.text.text);
				break;
			case SDL_TEXTEDITING:
				strcpy(compo, evt.edit.text);
				break;
			case SDL_KEYUP:
				switch (evt.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					done = 1;
					break;
				case SDLK_RETURN:
					sprintf(hello_msg, "Hello, %s!", text);
					break;
				case SDLK_BACKSPACE:
					backspace(text);
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
		}
		render(text, compo, hello_msg);
		SDL_Delay(20);
	}
}

int main(int argc, char* argv[])
{
	SDL_Window *window;
	SDL_Surface *prompt_surface;

	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();

	bigfont = TTF_OpenFont("Cyberbit.ttf", 40);
	mediumfont = TTF_OpenFont("Cyberbit.ttf", 32);
	smallfont = TTF_OpenFont("Cyberbit.ttf", 24);

	if (bigfont && mediumfont && smallfont)
	{
		window = SDL_CreateWindow("SDL 2.0 Text Input API Test",
								  SDL_WINDOWPOS_UNDEFINED,
								  SDL_WINDOWPOS_UNDEFINED,
								  800, 600,
								  SDL_WINDOW_SHOWN);
		renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

		prompt_surface = TTF_RenderUTF8_Blended(mediumfont, "What's your name?", black);
		prompt_texture = SDL_CreateTextureFromSurface(renderer, prompt_surface);
		prompt_w = prompt_surface->w; prompt_h = prompt_surface->h;
		SDL_FreeSurface(prompt_surface);

		SDL_StartTextInput();
		event_loop();

		SDL_DestroyTexture(prompt_texture);
		SDL_DestroyRenderer(renderer);

		TTF_CloseFont(bigfont);
		TTF_CloseFont(mediumfont);
		TTF_CloseFont(smallfont);
	}
	else
	{
		fprintf(stderr, "Couldn't load font.\n");
	}

	TTF_Quit();
	SDL_Quit();
	return 0;
}
