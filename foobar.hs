import Control.Monad.ST.Lazy
import Data.STRef

foo :: ST s [Int]
foo = do
  x <- strictToLazyST $ newSTRef 0
  bar x

bar x = do y <- strictToLazyST $ readSTRef x
           ys <- bar x
           return (y:ys)

main = print . take 5 $ runST foo

class (Eq a, Ord a) => Foo a where
  lol :: a -> a -> a


