(* merci filliatre
   pour les besoins de Prologin 2015 *)


let v = int_of_string Sys.argv.(1)
let e = int_of_string Sys.argv.(2)
let r = int_of_string Sys.argv.(3)
module Int = struct
type t = int
let compare = compare
let equal = (=)
let hash = Hashtbl.hash
let default = -1
end
open Graph
module G = Imperative.Digraph.ConcreteLabeled(Int)(Int)
module R = Rand.I(G)
let () = Random.self_init ()
let g = let f v1 v2 = Random.int 1000 + 2 in R.labeled f ~v ~e ()
let req_list = (* no duplication check *)
  let rec f = function
    | 0 -> []
    | n -> (Random.int v, Random.int v) :: f (n-1)
  in f r

module W = struct
type label = G.E.label
type t = int
let weight x = x
let zero = 0
let add = (+)
let compare = compare
end
module D = Path.Dijkstra(G)(W)
module C = Components.Make(G)

let () =
  assert (fst (C.scc g) = 1);
  Printf.printf "%d %d %d" v e r;
  let f e = Printf.printf "%d %d %d\n" (G.E.src e + 1) (G.E.dst e + 1) (G.E.label e) in
  G.iter_edges_e f g;
  let f (s,d) = Printf.printf "%d %d\n" (s+1) (d+1) in
  List.iter f req_list;

  if r <= 50 then begin
    print_newline ();
    (* dijkstra instead of floyd-warshall *)
    let f (v1, v2) = 
      let (_, w) = D.shortest_path g v1 v2 in
      Printf.printf "%d\n" w
    in
    List.iter f req_list
  end
  
    


