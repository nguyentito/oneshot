type 'a btree = Node of 'a btree * 'a * 'a btree | Leaf
                [@@deriving Iter]

let show_int_list = [%derive.Show: int list]

let test_btree =
  let lst = ref [] in
  iter_btree (fun x -> lst := x :: !lst)
             (Node (Node (Leaf, 0, Leaf), 1, Node (Leaf, 2, Leaf)));
  print_string @@ show_int_list !lst

type 'a ty = 'a * int list
             [@@deriving Iter]

