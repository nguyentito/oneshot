{-# LANGUAGE NoMonomorphismRestriction #-}

-- Mode d'emploi :
-- - Compilation :
--     ghc --make hexdiagram.hs
-- - Génération du dessin :
--     ./hexdiagram -o hexdiagram.svg -w 700

import Control.Lens hiding ((#))
import Diagrams.Prelude
import Diagrams.TwoD
import Diagrams.Backend.SVG.CmdLine

main = mainWith . hcat' (with & sep .~ 0.5) . map dia
       $ [ [ True, True, False, False, False, False ]
         , [ True, True, True,  True,  False, True ]
         , [ True, True, True,  True,  True,  True ]
         ]

-- Ce code est vraiment moche
-- il y a sans doute une meilleure manière de faire

dia :: [Bool] -> Diagram B R2
dia filled =
  (
    (
      (hexa (filled !! 0) 1 # snugY (-1))
      <>
      (
        (hexa (filled !! 1) 2 ||| hexa (filled !! 2) 3)
        # centerX
        # snugY 0 -- puts the origin at the bottom center vertex?
        # translateY (-1)
      )
    )
    # snugY (-1)
  )
  <>
  (
    (hexa (filled !! 3) 4 ||| hexa (filled !! 4) 5  ||| hexa (filled !! 5) 1)
    # centerX
    # snugY 1
  )
  
      

hexa filled n =
  (text (show n) # if filled then fc white else id)
  <>
  polygon (with & polyType .~ PolyRegular 6 1 & polyOrient .~ OrientV)
  # (if filled then fc blue else id)
