#include <stdio.h>

void read(int n, int m, int t[n][m])
{
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            scanf("%d", &t[i][j]);
}

void write_transpose(int n, int m, int t[n][m])
{
    for (int j = 0; j < m; j++) {
        for (int i = 0; i < n; i++)
            printf("%d ", t[i][j]);
        printf("\n");
    }
}


int main(int argc, char **argv)
{
    int n, m;
    scanf("%d %d\n", &n, &m);

    int t[n][m];
    read(n, m, t);
    write_transpose(n, m, t);

    int u[3][2] = { {1, 2}, {3, 4}, {5, 6} };
    write_transpose(3, 2, u);

    return 0;
}
