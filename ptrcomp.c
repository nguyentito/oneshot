#include <stdio.h>

struct S { int a; int b; };

int main () {
    struct S s1 = {0,0};
    struct S s2 = {0,0};
    if (&(s1.a) < &(s1.b)) printf("1");
    if (&s1 < &s2) printf("2");
    printf("\n");

    printf ("%d\n", 42 && 42);

    return 0;
}
